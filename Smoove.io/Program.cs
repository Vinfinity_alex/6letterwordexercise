﻿namespace letterwordexercise {
  public class Program {
    static void Main(string[] args) {

      var fileHandler = new FileHandler();
      
      var words = fileHandler.GetWords("input.txt");

      var fullWords = fileHandler.GetFullWords(
        words);

      var result = fileHandler.GetResults(
        fullWords,
        words);

      fileHandler.Output(result);
    }
  }
}