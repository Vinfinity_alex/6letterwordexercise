using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace letterwordexercise {
  public class FileHandler {

    public String[] GetWords(string path) {
      using (StreamReader sr = new StreamReader(path)) {
        var data = sr.ReadToEnd();
        return data
          .Split("\n")
          .Distinct()
          .Where(x => x.Length > 0)
          .ToArray();
      }
    }

    public String[] GetFullWords(
      String[] words) {
      return words
        .Where(x => x.Length == 6)
        .ToArray();
    }

    public List<RestulItem> GetResults(
      String[] fullWords,
      String[] words) {

      var result = new List<RestulItem>();
      
      var parts = words
        .Where(x => x.Length < 6)
        .ToArray();

      foreach (var part in parts) {
        //all words contains part
        var filteredWords = fullWords.Where(x => x.Contains(part)).ToArray();
        foreach (var filteredWord in filteredWords) {
          foreach (var anotherPart in parts.Where(x => x != part)) {
            if (part + anotherPart == filteredWord) {
              result.Add(new RestulItem(new []{part, anotherPart}, filteredWord));
            }
          }
        }
      }

      return result;
    }

    public void Output(
      IEnumerable<RestulItem> result) {
      foreach (var restulItem in result)
      {
        Console.Write("Parts: ");
        foreach (var restulItemItem in restulItem.Items) {
          Console.Write(restulItemItem);
          Console.Write(", ");
        }
        Console.Write("Full word: ");
        Console.WriteLine(restulItem.FullWord);
      }
      
      Console.WriteLine("Total count: " + result.Count());
    }
  }
}