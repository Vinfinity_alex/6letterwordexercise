namespace letterwordexercise {
  public class RestulItem {
    public string[] Items { get; }
    public string FullWord { get; }

    public RestulItem(string[] items, string fullWord) {
      Items = items;
      FullWord = fullWord;
    }
  }
}