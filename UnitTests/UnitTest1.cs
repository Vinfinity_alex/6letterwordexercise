using System;
using System.IO;
using System.Linq;
using letterwordexercise;
using Xunit;

namespace UnitTests {
  public class UnitTest1 {
    
    //GetWords
    [Fact]
    public void GetWords_ReadWordsFromInputFile_ReturnsArray() {
      var fileHandler = new FileHandler();

      var words = fileHandler.GetWords("input.txt");
      
      Assert.NotEmpty(words);
      Assert.NotNull(words.FirstOrDefault());
    }
    
    [Fact]
    public void GetWords_ReadWordsFromInputFile_ReturnsFileNotFoundException() {
      var fileHandler = new FileHandler();

      Assert.Throws<FileNotFoundException>(() => fileHandler.GetWords("test.txt"));
    }
    
    //Get6LettersWords
    [Fact]
    public void Get6LettersWords_Read6LettersWordsFromArray_ReturnsFilteredArray() {
      var fileHandler = new FileHandler();

      var words = fileHandler.GetWords("input.txt");
      
      var fullWords = fileHandler.GetFullWords(words);
      
      Assert.NotEmpty(fullWords);
      Assert.NotNull(fullWords.FirstOrDefault());
    }
    
    [Fact]
    public void Get6LettersWords_Read6LettersWordsFromNull_ReturnsArgumentNullException() {
      var fileHandler = new FileHandler();
      
      Assert.Throws<ArgumentNullException>(() => fileHandler.GetFullWords(null));
    }
    
    
    //GetResults
    [Fact]
    public void GetResults_GetWordPartsAndFullWordsArray_ReturnsArrayOfResultItems() {
      var fileHandler = new FileHandler();

      var words = fileHandler.GetWords("input.txt");
      
      var fullWords = fileHandler.GetFullWords(words);
      
      var result = fileHandler.GetResults(fullWords, words);
      
      Assert.NotEmpty(result);
      Assert.NotNull(result.FirstOrDefault());
      Assert.All(result, item => {
        Assert.True(item.Items.Length > 1);
        Assert.True(item.FullWord.Length == 6);
      });
    }
    
    [Fact]
    public void GetResults_GetNullAndWordsArray_ReturnsArrayOfResultItems() {
      var fileHandler = new FileHandler();

      var words = fileHandler.GetWords("input.txt");

      Assert.Throws<ArgumentNullException>(() => fileHandler.GetResults(null, words));
    }
    
    [Fact]
    public void GetResults_GetWordPartsAndNull_ReturnsArrayOfResultItems() {
      var fileHandler = new FileHandler();

      var words = fileHandler.GetWords("input.txt");
      
      var fullWords = fileHandler.GetFullWords(words);
      
      Assert.Throws<ArgumentNullException>(() => fileHandler.GetResults(fullWords, null));
    }
    
    [Fact]
    public void GetResults_GetNullAndNull_ReturnsArrayOfResultItems() {
      var fileHandler = new FileHandler();

      Assert.Throws<ArgumentNullException>(() => fileHandler.GetResults(null, null));
    }
  }
}